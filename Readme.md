measurement_graphs

===============================================================================================

Plot Distance(x)&RSSI(y) distance(x)&count(y) over several data sets (measurements) from a json formatted data file.

![](images/screenshot.png?raw=true)

===============================================================================================

sfml-plot developed by bechu is used for graphing.. https://github.com/bechu/sfml-plot.

I frankensteined it.

Additional functionality includes:
+ Plot variables based on x-value such that a point is x-positioned based on its x-property and not its order in the list.
+ Graph range can be modified to not depend on highest-values such as to add to readability.
- Not made with expandability in mind. It was modified until it worked for the purposes of this implementation. This also means it is not optimized.


You can press the Spacebar to switch between checking the RSSI graph of one dataset, or checking the overall average data graph of all data sets.

When looking at the RSSI data of one data set, you can use the arrow keys to switch between data sets.

When looking at the overall average data graph of all data sets, you can press S to switch between count and RSSI as the y-variable.

===============================================================================================

You need to have SFML 2.0 or later installed.

I recommend using SFML-2.5.1 if you don't want to use
newer versions to edit and expand upon the program.

When you've installed SFML 2.0 or later, you need to edit the directory in C/C++->General->Additional Include Directories to fit your equivalent filepath AND version. An example is "C:\Program Files (x86)\SFML-2.5.1\include" but since you may have a newer version, it may look different. It may look like "D:\SFML-2.6.0\include" or something.

The same process goes to edit the Linker->General->Additional Library Directories to fit your filepath, example is "C:\Program Files(x86)\SFML-2.5.1\lib"

You need to add these to the debug Linker->Input->Additional Dependencies:
sfml-graphics-d.lib;
sfml-window-d.lib;
sfml-system-d.lib;

You need to add these to the release Linker->Input->Additional Dependencies:
sfml-graphics.lib;
sfml-window.lib;
sfml-system.lib;