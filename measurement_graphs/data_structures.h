#pragma once
/*! Class to represent a position. */

class Position {
	const std::string dmsStrLat_; /*<! Latitude ddmm.mmmmm string   */
	const std::string dmsStrLon_; /*<! Longitude dddmm.mmmmm string */

public:
	Position(std::string dmsStrLat, std::string dmsStrLon)
	: dmsStrLat_(dmsStrLat), dmsStrLon_(dmsStrLon)
	{
		//! \todo create a conversion to decimal coordinates.
	};
	~Position() {};

	std::string dmsStrLat() {
		return dmsStrLat_;
	}
	
	std::string dmsStrLon() {
		return dmsStrLon_;
	}

};



/*! Class to represent a data point in a measurement.
 *
 * A data point represents a point in which a central device has received data from
 * a peripheral device. This abstraction has the corresponding positions of the
 * devices at that time, as well as the numerical counter within the measurement procedure
 * to identify the data point.
 */

class DataPoint {
	int counter_;				/*<! Identifies which data point in a data set. */
	Position posCentral_;		/*<! Position of central device at measurement. */
	Position posPeripheral_;	/*<! Position of peripheral device at measurement. */
	float distance_ = 0.0f;		/*<! Distance between central and peripheral positions. */
	float RSSI_;				/*<! Perceived signal strength indicator of measurement. */
public:
	DataPoint(int counter,
			  Position posCentral,
			  Position posPeripheral,
			  float RSSI
			  ) :
			  counter_(counter),
			  posCentral_(posCentral),
			  posPeripheral_(posPeripheral),
			  RSSI_(RSSI)
	{
		//! \todo fix distance calculation here, or call to Position dist.
	};

	~DataPoint() {};

	int counter() {
		return this->counter_;
	};

	Position central() {
		return this->posCentral_;
	};

	Position peripheral() {
		return this->posPeripheral_;
	};

	float distance() {
		return this->distance_;
	};

	float RSSI() {
		return this->RSSI_;
	}

};

class DataSet {
	uint8_t dataSetId_;	/*<! Identifies which set of data points. */
	uint8_t count_ = 0;		/*<! Tells amount of data points in set. */

	std::vector<DataPoint*> dataPoints_; /*<! Vector of data points.*/

public:

	DataSet() {};
	~DataSet() {

		for (int i = 0; i < dataPoints_.size(); i++) {
			delete dataPoints_[i];
		}
	};

	void deletePoints() {

		this->count_ = 0;

		for (int i = dataPoints_.size() - 1; i >= 0; i--) {
			dataPoints_.erase(dataPoints_.begin() + i);
		}

	}

	void addDataPoint(DataPoint* new_point) {

		this->count_ += 1;

		this->dataPoints_.push_back(new_point);
	}

	uint8_t dataSetId() {
		return this->dataSetId_;
	};

	uint8_t count() {
		return this->count_;
	};

	std::vector<DataPoint*> dataPoints() {
		return dataPoints_;
	}

};