#include "pch.h"
#include <stdio.h>
#include <cmath>		
#include <string>		// strings
#include <iostream>		// cin, cout
#include <sstream>		// string stream for easy separation of string info.
#include <fstream>		// reading from a file
#include <iomanip>		// setw
#include <vector>		// vectors
#include "json.hpp"		// nlohmann's lib
#include <cstdio>		// popen
#include <cstring>		// memset
#include <algorithm>	// sort

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "src/plot.h" // This library has been molested to make RSSI(METERS) and count(METERS) graphs a thing.

constexpr auto MAX_METERS = 680;	         /*! <Max meters on the Y axis. */
constexpr auto MAX_MEASUREMENTS = 100;       /*! <Max amount of measurements per data set. */
constexpr auto MAX_DATASETS = 10000;	     /*! <Max data sets. Used because sfmlplot wants it. The number 10000 has no significance. */
constexpr auto maff_PI = 3.14159265359;      /*! <Pie. */
constexpr auto WINDOW_HEIGHT = 800;          /*! <Height of the SFML window. */
constexpr auto WINDOW_WIDTH = 1200;          /*! <Width of the SFML window. */
constexpr auto GRAPH_HEIGHT = WINDOW_HEIGHT; /*! <Graph will expand to cover the whole window. */
constexpr auto GRAPH_WIDTH = WINDOW_WIDTH;   /*! <Graph will expand to cover the whole window. */

using namespace nlohmann;

/*! Utility function for converting degrees to radians */
long double toRadians(const long double degree)
{
	long double one_deg = (maff_PI) / 180;
	return (one_deg * degree);
}

/*! Source for distanceMeters: https://www.geeksforgeeks.org/program-distance-two-points-earth/*/
long double distanceMeters(long double lat1, long double long1,
	long double lat2, long double long2)
{
	// Convert the latitudes  
	// and longitudes 
	// from degree to radians. 
	lat1 = toRadians(lat1);
	long1 = toRadians(long1);
	lat2 = toRadians(lat2);
	long2 = toRadians(long2);

	// Haversine Formula 
	long double dlong = long2 - long1;
	long double dlat = lat2 - lat1;

	long double ans = pow(sin(dlat / 2), 2) +
		cos(lat1) * cos(lat2) *
		pow(sin(dlong / 2), 2);

	ans = 2 * asin(sqrt(ans));

	// Radius of Earth in  
	// Kilometers, R = 6371 
	// Use R = 3956 for miles 
	long double R = 6371;

	// Calculate the result 
	ans = ans * R;

	return ans * 1000;
}

long double latDMToDegrees(std::string dm_lat) {
	int mm = (dm_lat[0] - 48) * 10;
	mm += dm_lat[1] - 48;
	long double lat = mm + std::stod(dm_lat.substr(2, 9)) / 60;
	return lat;
}

long double lonDMToDegrees(std::string dm_lon) {
	int mm = (dm_lon[0] - 48) * 10;
	mm += dm_lon[1] - 48;
	long double lon = mm + std::stod(dm_lon.substr(2, 10)) / 60;
	return lon;
}

class Coordinate {

	std::string latitude_; //! <ddmm.mmmmm latitude degrees minutes
	std::string longitude_; //! <dddmm.mmmmm longitude degrees minutes

public:
	Coordinate(std::string lat, std::string lon) :
		latitude_(lat), longitude_(lon) {};
	~Coordinate() {};

	std::string latitude() {
		return latitude_;
	}

	std::string longitude() {
		return longitude_;
	}


	/*! Distance to another point using the haversine algorithm. */
	long double distTo(Coordinate* otherCoord) {
		return distanceMeters(
			latDMToDegrees(this->latitude_),
			lonDMToDegrees(this->longitude_),
			latDMToDegrees(otherCoord->latitude()),
			lonDMToDegrees(otherCoord->longitude())
		);
	}

};

class DataPoint {

	Coordinate* peripheralCoord_;	//! <Coordinate of the peripheral device.
	Coordinate* centralCoord_;		//! <Coordinate of the central device.
	int RSSI_;	//! <RSSI value received over measurement.
	long double distance_; //! <Distance calculated between the coordinates.

public:
	DataPoint(Coordinate* cCoord, Coordinate* pCoord, int RSSI) :
		centralCoord_(cCoord), peripheralCoord_(pCoord), RSSI_(RSSI) {

		this->distance_ = pCoord->distTo(cCoord);

	};

	~DataPoint() {};

	int RSSI() {
		return this->RSSI_;
	}

	long double distance() {
		return this->distance_;
	}

};

class DataSet : sf::Drawable {

	long double avgRSSI_; //! <Average RSSI.
	long double avgDistance_; //! <Average distance.
	int count_; //! <Amount of data points.
	int setID_; //! <ID of data set.

	bool averaged = false;
	sf::plot::Plot plot_;
	sf::Font font_;
	sf::Text stats_;

	std::vector<DataPoint*> dataPoints_;

public:

	DataSet(int setID) : setID_(setID) {
		
		plot_.setSize(sf::Vector2f(GRAPH_WIDTH, GRAPH_HEIGHT));
		plot_.setTitle("RSSI over time");
		plot_.setFont("font.ttf");
		plot_.setXLabel("Adv. packet");
		plot_.setYLabel("RSSI");
		plot_.setBackgroundColor(sf::Color(122, 122, 122));
		plot_.setTitleColor(sf::Color(255, 255, 255));
		plot_.setPosition(0, 0);
		sf::plot::Curve &curve = plot_.createCurve("rssi", sf::Color::Red);
		curve.setFill(true);
		curve.setThickness(2);
		curve.setColor(sf::Color(255, 255, 255));
		curve.setLimit(MAX_MEASUREMENTS);
	
	};
	~DataSet() {};

	/*! Adds data point and increases count.*/
	void addDataPoint(DataPoint* newPoint) {
		this->averaged = false;
		this->count_++;
		this->dataPoints_.push_back(newPoint);
		this->plot_.getCurve("rssi").addValue(newPoint->RSSI());
		this->plot_.prepareRSSI();
	}

	/*! Calculates the average values of RSSI and Distance.*/
	void calculateAverages() {
		int tmp_RSSI = 0;
		long double tmp_distance = 0;

		for (int i = 0; i < this->dataPoints_.size(); i++) {
			tmp_RSSI += dataPoints_[i]->RSSI();
			tmp_distance += dataPoints_[i]->distance();
		}

		this->avgRSSI_ = ((long double)tmp_RSSI) / count_;
		this->avgDistance_ = tmp_distance / count_;

		this->averaged = true;
	}

	/*! Amount of data points. */
	int count() {
		return count_;
	}

	/*! ID of the set of data points. */
	int setID() {
		return setID_;
	}

	sf::plot::Plot plot() {
		return this->plot_;
	}

	sf::Text stats() {
		return stats_;
	}

	/*! Average RSSI. Calls calculation if not done yet. */
	long double avgRSSI() {

		if (this->averaged != true) {
			this->calculateAverages();
		}

		return avgRSSI_;
	}

	/*! Average distance. Calls calculation of not done yet. */
	long double avgDistance() {

		if (this->averaged != true) {
			this->calculateAverages();
		}

		return avgDistance_;
	}

	/*! Used to sort. */
	bool lessThan(DataSet* other) {
		return this->avgDistance_ < other->avgDistance();
	}

	/*! Prints relevant variables. */
	void printVars() {
		printf("\nsetID: %d\n", this->setID_);
		printf("count: %d\n", this->count_);
		printf("Average distance: %.5f\n", this->avgDistance());
		printf("Average RSSI: %.5f\n", this->avgRSSI());
	}

	/*! Prepares stat text for drawing. */
	void prepareText() {
		if (!font_.loadFromFile("font.ttf"))
		{
			throw;
		}

		this->stats_.setFont(this->font_);

		std::string tmp_str = "Count: " + std::to_string(this->count_) + '\n';
		tmp_str += "Distance(avg): " + std::to_string(this->avgDistance()) + " meters.\n";
		tmp_str += "RSSI(avg): " + std::to_string(this->avgRSSI()) + " dBm.";
		this->stats_.setString(tmp_str);

		stats_.setCharacterSize(36);
		stats_.setFillColor(sf::Color(0,0,0));
		stats_.setPosition(
			500, 200
		);
	}

	/*! Drawing func. */
	void draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		target.draw(plot_, states);
	}

};

struct ComparisonDataSetDistance {
	bool const operator()( DataSet* lhs, DataSet* rhs) {
		return (lhs->avgDistance()) < (rhs->avgDistance());
	}
};

class Summary {

	sf::plot::Plot plotRSSI_;
	sf::plot::Plot plotCount_;

public:

	std::vector<DataSet*> dataSets_;


	Summary() {

	};
	~Summary() {

	};

	/*! Prepares a plot and a curve dependent on RSSI and distance. */
	void plotGraphRSSI() {
		plotRSSI_.setSize(sf::Vector2f(GRAPH_WIDTH, GRAPH_HEIGHT));
		plotRSSI_.setTitle("");
		plotRSSI_.setFont("font.ttf");
		plotRSSI_.setXLabel("Distance (meters)");
		plotRSSI_.setYLabel("RSSI");
		plotRSSI_.setBackgroundColor(sf::Color(122, 122, 122));
		plotRSSI_.setTitleColor(sf::Color(255, 255, 255));
		plotRSSI_.setPosition(0, 0);
		sf::plot::Curve &curve = plotRSSI_.createCurve("datasetmeter", sf::Color::Red);
		curve.setFill(false);
		curve.setThickness(2);
		curve.setColor(sf::Color(255, 255, 255));
		curve.setLimit(MAX_DATASETS);

		for (int i = 0; i < this->dataSets_.size(); i++) {
			this->plotRSSI_.getCurve("datasetmeter").addValue2( sf::Vector2f(dataSets_[i]->avgDistance(), dataSets_[i]->avgRSSI()) );
		}
		this->plotRSSI_.prepareRSSIDISTANCE(MAX_METERS);
	}

	/*! Prepares a plot and a curve dependent on count and distance. */
	void plotGraphCount() {
		plotCount_.setSize(sf::Vector2f(GRAPH_WIDTH, GRAPH_HEIGHT));
		plotCount_.setTitle("");
		plotCount_.setFont("font.ttf");
		plotCount_.setXLabel("Distance (meters)");
		plotCount_.setYLabel("Count");
		plotCount_.setBackgroundColor(sf::Color(122, 122, 122));
		plotCount_.setTitleColor(sf::Color(255, 255, 255));
		plotCount_.setPosition(0, 0);
		sf::plot::Curve &curve = plotCount_.createCurve("datasetmeter", sf::Color::Red);
		curve.setFill(false);
		curve.setThickness(2);
		curve.setColor(sf::Color(255, 255, 255));
		curve.setLimit(MAX_DATASETS);

		for (int i = 0; i < this->dataSets_.size(); i++) {
			this->plotCount_.getCurve("datasetmeter").addValue2(sf::Vector2f(dataSets_[i]->avgDistance(), dataSets_[i]->count()));
		}
		this->plotCount_.prepareCountDISTANCE(MAX_METERS);
	}

	sf::plot::Plot plotRSSI() {
		return this->plotRSSI_;
	}

	sf::plot::Plot plotCount() {
		return this->plotCount_;
	}

	/*! Sorts the data sets by distance. c.b.a. implementing another sort atm.*/
	void sortByDistance() {
		std::sort(dataSets_.begin(), dataSets_.end(), ComparisonDataSetDistance());
	}


	/*! Calls the datasets print function. */
	void printVars() {
		for (int i = 0; i < this->dataSets_.size(); i++) {
			this->dataSets_[i]->printVars();
		}
	}

};


enum viewMode {
	e_viewMode_oneSet = 0,
	e_viewMode_summary
};

int main()
{

	std::string filename = "logsarehere.txt";
	std::ifstream i(filename);
	json wholeJson;

	i >> wholeJson;

	json sets = wholeJson["list of adv pkt"];

	std::vector<DataSet*> dataSets;
	Summary summary;

	DataSet* tmp_dataSet;
	DataPoint* tmp_dataPoint;
	Coordinate* tmp_cCoord;
	Coordinate* tmp_pCoord;
	int tmp_RSSI;
	
	for (auto& dataSet : sets) {

		tmp_dataSet = new DataSet(dataSet["set id"]);
		summary.dataSets_.push_back(tmp_dataSet);

		for (auto& dataPoint : dataSet["set"]) {
			//std::cout << "adv id: " << dataPoint["adv id"] << std::endl;
			//std::cout << std::setw(2) << dataPoint["data"][0] << std::endl;

			tmp_cCoord = new Coordinate(dataPoint["data"][0]["CentralGPSLat"], dataPoint["data"][0]["CentralGPSLon"]);
			tmp_pCoord = new Coordinate(dataPoint["data"][0]["peripheralGPSLat"], dataPoint["data"][0]["peripheralGPSLon"]);
			tmp_RSSI = dataPoint["data"][0]["RSSI"];

			tmp_dataPoint = new DataPoint(tmp_cCoord, tmp_pCoord, tmp_RSSI);
			tmp_dataSet->addDataPoint(tmp_dataPoint);
		}

		tmp_dataSet->prepareText();
	}

	
	std::cout << "\n/-------------------SORTED BY SETID: ----------------------/";
	summary.printVars();
	std::cout << "\n/-------------------SORTED BY DISTANCE: -------------------/";
	summary.sortByDistance();
	summary.printVars();

	summary.plotGraphRSSI();
	summary.plotGraphCount();

	bool countOverRSSI = true;

	sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "SFMLplot RSSI", sf::Style::Default);

	viewMode vMode = e_viewMode_summary;

	int selectedDataSet = 0;
	int selectedDataSetMaxI = summary.dataSets_.size() - 1;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();

			if (event.type == sf::Event::KeyReleased)
			{
				if (vMode == e_viewMode_oneSet) {
					if (event.key.code == sf::Keyboard::Up
						||
						event.key.code == sf::Keyboard::Right) {

						if (selectedDataSet + 1 > selectedDataSetMaxI) {
							selectedDataSet = 0;
						}
						else {
							selectedDataSet++;
						}
					}

					if (event.key.code == sf::Keyboard::Down
						||
						event.key.code == sf::Keyboard::Left) {

						if (selectedDataSet - 1 < 0) {
							selectedDataSet = selectedDataSetMaxI;
						}
						else {
							selectedDataSet--;
						}
					}
				}
				else {
					if (event.key.code == sf::Keyboard::S) {
						countOverRSSI = !countOverRSSI;
					}
				}
				
				if (event.key.code == sf::Keyboard::Space) {
					if (vMode == e_viewMode_oneSet) {
						vMode = e_viewMode_summary;
					}
					else {
						vMode = e_viewMode_oneSet;
					}
				}
			}

			
		}

		window.clear();

		switch (vMode) {
		case e_viewMode_oneSet:
			window.draw(summary.dataSets_[selectedDataSet]->plot());
			window.draw(summary.dataSets_[selectedDataSet]->stats());
			break;
		case e_viewMode_summary:
			if (countOverRSSI) {
				window.draw(summary.plotCount());
			}
			else {
				window.draw(summary.plotRSSI());
			}
			break;
		}

		window.display();
	}
}